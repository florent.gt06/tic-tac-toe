const board = document.getElementById('board'); // Récupère la grille dans le DOM
const cells = document.querySelectorAll('.cell'); // Récupère tous les divs de la grille dans le DOM
let timer = '';
let currentPlayer = 'X'; // Le joueur actuel est X au début de la partie
let difficulty = 'easy';
let xWins = 0;
let oWins = 0;

board.addEventListener('click', handleClick); // Écoute les clics sur la grille

const resetButton = document.getElementById('reset-button');
resetButton.addEventListener('click', resetBoard);

const resetPointsButton = document.getElementById('reset-points');
resetPointsButton.addEventListener('click', resetPoints);

const modeInputs = document.querySelectorAll('input[name=mode]');

modeInputs.forEach(function(modeInput) {
    modeInput.addEventListener('change', handleModeChange);
});

const difficultySelect = document.getElementById('difficulty-select');
const difficultyDiv = document.getElementById('difficulty-div');
difficultySelect.addEventListener('change', event => {
    difficulty = event.target.value;
});


let gameMode = 'player'; // Le mode de jeu par défaut est 'player'

function handleModeChange(event) {
    gameMode = event.target.value;
    difficultyDiv.style.display = 'none';
    if (gameMode === 'computer'){
        difficultyDiv.style.display = 'block';
        currentPlayer = Math.random() < 0.5 ? 'O' : 'X';
        showMessage(`C'est le joueur ${currentPlayer} qui commence.`, 'success', false)
        if (currentPlayer === 'O') {
            makeComputerMove();
        }
    }
}

function handleClick(event) {
    // Si la cellule est déjà occupée ou si la partie est terminée, on ne fait rien
    if (event.target.textContent || checkWin()) {
        return;
    }

    // Le joueur courant joue sur la cellule cliquée
    event.target.textContent = currentPlayer;

    // Vérifie si le joueur courant a gagné
    const result = checkWin(currentPlayer);
    if (result) {
        // La partie est terminée
        if (result === 'X') {
            // Le joueur X a gagné
            showMessage('Le joueur X a gagné!', 'success');
        } else if (result === 'O') {
            // Le joueur O a gagné
            showMessage("Le joueur O a gagné!", 'success');
        } else if (result === 'T') {
            // Egalité
            showMessage("Egalité!", 'warning');
        }
    } else if (gameMode === 'computer') {
        // L'IA joue
        makeComputerMove();

        // Vérifie si l'IA a gagné
        const result = checkWin('O');
        if (result) {
            // La partie est terminée
            if (result === 'O') {
                // L'IA a gagné
                showMessage("L'IA a gagné!", 'danger');
            } else if (result === 'T') {
                // Egalité
                showMessage("Egalité!", 'warning');
            }
        }
    } else {
        // Change le joueur courant
        currentPlayer = (currentPlayer === 'X') ? 'O' : 'X';
    }
}

function checkWin(player) {
    const cellsArray = Array.from(cells);
    // Vérifie si le joueur a gagné en parcourant chaque ligne, colonne et diagonale de la grille
    for (let i = 0; i < 3; i++) {
        // Récupère les cellules de la ligne courante
        const row = cellsArray.slice(i * 3, (i + 1) * 3);
        // Récupère les cellules de la colonne courante
        const col = [cellsArray[i], cellsArray[i + 3], cellsArray[i + 6]];
        // Récupère les cellules de la diagonale courante
        const diag = i === 0 ? [cellsArray[0], cellsArray[4], cellsArray[8]] : [cellsArray[2], cellsArray[4], cellsArray[6]];

        // Vérifie si toutes les cellules de la ligne, de la colonne ou de la diagonale sont occupées par le joueur
        if (row.every(cell => cell.textContent === player) || col.every(cell => cell.textContent === player) || diag.every(cell => cell.textContent === player)) {
            if (player === 'X'){
                xWins++;
            }else{
                oWins++;
            }
            addPoints(player);
            return player;
        }
    }

    // Vérifie si la grille est complète (c'est-à-dire si toutes les cellules sont occupées)
    if (cellsArray.every(cell => cell.textContent)) {
        return 'T';
    }

    // Sinon, la partie n'est pas terminée
    return null;
}


function resetBoard() {
    cells.forEach(cell => {
        cell.textContent = ''; // Efface le contenu de chaque cellule
    });
    currentPlayer = 'X'; // Remet le joueur actuel à X pour le prochain tour
    clearTimeout(timer);
    const messageElement = document.getElementById('message');
    messageElement.style.display = 'none';
    if (gameMode === 'computer'){
        currentPlayer = Math.random() < 0.5 ? 'O' : 'X';
        showMessage(`C'est le joueur ${currentPlayer} qui commence.`, 'success', false)
        if (currentPlayer === 'O') {
            makeComputerMove();
        }
    }
}

function makeComputerMove() {
    const cellsArray = Array.from(cells);
    switch (difficulty) {
        case 'easy':
            // Choisit une cellule au hasard
            const emptyCells = cellsArray.filter(cell => !cell.textContent);
            const randomIndex = Math.floor(Math.random() * emptyCells.length);
            emptyCells[randomIndex].textContent = 'O';
            break;
        case 'medium':
            const winningMoveMedium = getWinningMove();
            if (winningMoveMedium) {
                winningMoveMedium.textContent = 'O';
                finTour();
                return;
            }
            randomCell()
            break;
        case 'hard':
            const winningMoveHard = getWinningMove();
            if (winningMoveHard) {
                winningMoveHard.textContent = 'O';
                finTour();
                return;
            }

            const blockingMove = getBlockingMove();
            if (blockingMove) {
                blockingMove.textContent = 'O';
                finTour();
                return;
            }
            randomCell()
            break;
    }
    currentPlayer = 'X';
}

function randomCell() {
    const cellsArray = Array.from(cells);
    // Choisit une cellule au hasard
    const emptyCells = cellsArray.filter(cell => !cell.textContent);
    const randomIndex = Math.floor(Math.random() * emptyCells.length);
    emptyCells[randomIndex].textContent = 'O';
}

function finTour(){
    // Vérifie si l'IA a gagné
    if (checkWin()) {
        alert(`L'IA a gagné!`);
        resetBoard();
    } else {
        // Si l'IA n'a pas gagné, c'est au joueur X de jouer
        currentPlayer = 'X';
    }
}

function getWinningMove() {
    const cellsArray = Array.from(cells);
    // Parcourt chaque ligne, colonne et diagonale de la grille
    for (let i = 0; i < 3; i++) {
        // Récupère les cellules de la ligne courante
        const row = cellsArray.slice(i * 3, (i + 1) * 3);
        // Récupère les cellules de la colonne courante
        const col = [cellsArray[i], cellsArray[i + 3], cellsArray[i + 6]];
        // Récupère les cellules de la diagonale courante
        const diag = i === 0 ? [cellsArray[0], cellsArray[4], cellsArray[8]] : [cellsArray[2], cellsArray[4], cellsArray[6]];

        // Vérifie si une des cellules de la ligne, de la colonne ou de la diagonale est vide et si les deux autres sont occupées par l'IA
        for (let j = 0; j < 3; j++) {
            if (row[j].textContent === 'O' && row[(j + 1) % 3].textContent === 'O' && !row[(j + 2) % 3].textContent) {
                return row[(j + 2) % 3];
            } else if (col[j].textContent === 'O' && col[(j + 1) % 3].textContent === 'O' && !col[(j + 2) % 3].textContent) {
                return col[(j + 2) % 3];
            } else if (diag[j].textContent === 'O' && diag[(j + 1) % 3].textContent === 'O' && !diag[(j + 2) % 3].textContent) {
                return diag[(j + 2) % 3];
            }
        }
    }

    return null;
}

function getBlockingMove() {
    const cellsArray = Array.from(cells);
    // Parcourt chaque ligne, colonne et diagonale de la grille
    for (let i = 0; i < 3; i++) {
        // Récupère les cellules de la ligne courante
        const row = cellsArray.slice(i * 3, (i + 1) * 3);
        // Récupère les cellules de la colonne courante
        const col = [cellsArray[i], cellsArray[i + 3], cellsArray[i + 6]];
        // Récupère les cellules de la diagonale courante
        const diag = i === 0 ? [cellsArray[0], cellsArray[4], cellsArray[8]] : [cellsArray[2], cellsArray[4], cellsArray[6]];

        // Vérifie si une des cellules de la ligne, de la colonne ou de la diagonale est vide et si les deux autres sont occupées par l'IA
        for (let j = 0; j < 3; j++) {
            if (row[j].textContent === 'X' && row[(j + 1) % 3].textContent === 'X' && !row[(j + 2) % 3].textContent) {
                return row[(j + 2) % 3];
            } else if (col[j].textContent === 'X' && col[(j + 1) % 3].textContent === 'X' && !col[(j + 2) % 3].textContent) {
                return col[(j + 2) % 3];
            } else if (diag[j].textContent === 'X' && diag[(j + 1) % 3].textContent === 'X' && !diag[(j + 2) % 3].textContent) {
                return diag[(j + 2) % 3];
            }
        }
    }

    return null;
}

function showMessage(text, couleur = 'success', wReset = true) {
    // Récupère l'élément div
    const messageElement = document.getElementById('message');
    // Modifie le contenu du message
    messageElement.textContent = text;
    // Affiche le message
    messageElement.style.display = 'block';
    switch (couleur){
        case "success":
            messageElement.style.backgroundColor = 'darkgreen';
            break;
        case "danger":
            messageElement.style.backgroundColor = 'red';
            break;
        case "warning":
            messageElement.style.backgroundColor = 'orange';
            break;
    }

    if (wReset){
        timer = setTimeout(function () {
            // Cache le message
            resetBoard()
        }, 4000)
    }else{
        setTimeout(function () {
            messageElement.style.display = 'none';
        }, 1500)
    }
}

function addPoints(player) {
    let playerWins = document.getElementById(player.toLowerCase() + '-wins');
    if (player === 'X'){
        playerWins.textContent = xWins.toString();
    }else{
        playerWins.textContent = oWins.toString();
    }

}

function resetPoints() {
    xWins = 0;
    oWins = 0;
    let playerOWin = document.getElementById('o-wins');
    let playerXWin = document.getElementById('x-wins');
    playerOWin.textContent = oWins;
    playerXWin.textContent = xWins;
}
